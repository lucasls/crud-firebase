import 'package:crudfire/lista_produtos.dart';
import 'package:flutter/material.dart';

void main() async {
  runApp(
    MaterialApp(
      title: 'Firestore - Flutter',
      theme: ThemeData(
        primaryColor: Colors.orange,
      ),
      home: ListaProdutos(),
    ),
  );
}
