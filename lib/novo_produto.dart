import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:crudfire/model/produto.dart';
import 'package:flutter/material.dart';

class NovoProduto extends StatefulWidget {
  final Produto produto;
  NovoProduto(this.produto);

  @override
  _NovoProdutoState createState() => _NovoProdutoState();
}

class _NovoProdutoState extends State<NovoProduto> {
  final db = Firestore.instance;
  TextEditingController _nomeController;
  TextEditingController _precoController;

  @override
  void initState() {
    super.initState();
    _nomeController = TextEditingController(text: widget.produto.nome);
    _precoController = TextEditingController(text: widget.produto.preco);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edita/Inclui - Produtos"),
        centerTitle: true,
      ),
      body: Container(
        margin: EdgeInsets.all(15.0),
        alignment: Alignment.center,
        child: Column(
          children: <Widget>[
            TextField(
              controller: _nomeController,
              decoration: InputDecoration(labelText: 'Nome'),
            ),
            TextField(
              controller: _precoController,
              decoration: InputDecoration(labelText: 'Preco'),
            ),
            RaisedButton(
              child:
                  (widget.produto.id != null) ? Text('Atualiza') : Text('Novo'),
              textColor: Colors.black,
              color: Colors.orange,
              onPressed: () {
                if (widget.produto.id != null) {
                  db
                      .collection("produtos")
                      .document(widget.produto.id)
                      .setData({
                    "nome": _nomeController.text,
                    "preco": _precoController.text,
                  });
                  Navigator.pop(context);
                } else {
                  db
                      .collection("produtos")
                      .document(widget.produto.id)
                      .setData({
                    "nome": _nomeController.text,
                    "preco": _precoController.text,
                  });
                  Navigator.pop(context);
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
