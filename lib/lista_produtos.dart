import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:crudfire/novo_produto.dart';
import 'package:flutter/material.dart';
import 'model/produto.dart';

class ListaProdutos extends StatefulWidget {
  @override
  _ListaProdutosState createState() => _ListaProdutosState();
}

class _ListaProdutosState extends State<ListaProdutos> {
  List<Produto> items;
  var db = Firestore.instance;

  StreamSubscription<QuerySnapshot> produtoInscricao;

  @override
  void initState() {
    super.initState();

    items = List();
    produtoInscricao?.cancel();

    produtoInscricao = db.collection("produtos").snapshots().listen(
      (snapshot) {
        final List<Produto> produtos = snapshot.documents
            .map(
              (documentSnapshot) => Produto.fromMap(
                  documentSnapshot.data, documentSnapshot.documentID),
            )
            .toList();
        setState(
          () {
            this.items = produtos;
          },
        );
      },
    );
  }

  @override
  void dispose() {
    produtoInscricao?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lista Produtos"),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: StreamBuilder<QuerySnapshot>(
              stream: getListaProdutos(),
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.waiting:
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  default:
                    List<DocumentSnapshot> documentos = snapshot.data.documents;
                    return ListView.builder(
                      itemCount: documentos.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          title: Text(items[index].nome,
                              style: TextStyle(fontSize: 22)),
                          subtitle: Text(
                            items[index].preco,
                            style: TextStyle(fontSize: 22),
                          ),
                          leading: Column(
                            children: <Widget>[
                              IconButton(
                                  icon: const Icon(Icons.delete_forever),
                                  onPressed: () {
                                    _deletaProduto(
                                        context, documentos[index], index);
                                  })
                            ],
                          ),
                          onTap: () =>
                              _navegarParaProduto(context, items[index]),
                        );
                      },
                    );
                }
              },
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.orange,
        onPressed: () => _criarNovoProduto(context, Produto(null, '', '')),
      ),
    );
  }

  Stream<QuerySnapshot> getListaProdutos() {
    return Firestore.instance.collection('produtos').snapshots();
  }

  void _deletaProduto(
      BuildContext context, DocumentSnapshot doc, int position) async {
    db.collection("produtos").document(doc.documentID).delete();
    setState(() {
      items.removeAt(position);
    });
  }

  void _navegarParaProduto(BuildContext context, Produto produto) async {
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => NovoProduto(produto),
      ),
    );
  }

  void _criarNovoProduto(BuildContext context, Produto produto) async {
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => NovoProduto(Produto(null, '', '')),
      ),
    );
  }
}
